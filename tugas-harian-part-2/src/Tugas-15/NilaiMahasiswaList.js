import React, { useEffect, useContext, useState} from "react";
import { useHistory } from "react-router-dom";
import { NilaiMahasiswaContext } from "../Tugas-13/NilaiMahasiswaContext";

const NilaiMahasiswaList = () => {
    const { dataMahasiswa, fetchStatus, setFetchStatus, functions, addMessage } = useContext(NilaiMahasiswaContext)
    const { fetchData, functionDelete, functionEdit, functionIndeksNilai } = functions;
    const [deleteMessage, setDeleteMessage] = useState("invisible");
    const handleEdit = (event) => {
        let idMahasiswa = event.target.value;
        functionEdit(idMahasiswa);
    }
    const handleDelete = (event) => {
        let idMahasiswa = event.target.value;
        functionDelete(idMahasiswa);
        setDeleteMessage("visible");
        setTimeout(()=> {
            setDeleteMessage("invisible");
        },3000)
    }
    useEffect( () => {
        fetchData()
        setFetchStatus(false);
      }, [fetchStatus,setFetchStatus])
    
    let history = useHistory();
    function handleClick() {
        history.push("/tugas-15/create");
    }
    return (
        <>
        <div className={`${addMessage} fixed bottom-5 left-5 right-0 w-80 bg-green-200 border-green-600 text-green-600 border-l-4 p-4`} role="alert">
            <p className="font-bold">
                Berhasil
            </p>
            <p>
                Data berhasil ditambahkan
            </p>
        </div>
        <div className={`${deleteMessage} fixed bottom-5 left-5 right-0 w-80 bg-red-200 border-red-600 text-red-600 border-l-4 p-4`} role="alert">
            <p className="font-bold">
                Berhasil
            </p>
            <p>
                Data berhasil dihapus.
            </p>
        </div>
        <div className="w-fit mx-auto grid">
        <button onClick={handleClick} className="my-2 mx-auto w-52 py-2 px-4  bg-cyan-400 hover:bg-cyan-600 focus:ring-cyan-400 focus:ring-offset-cyan-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">Create Data</button>
        <table className="my-2 table p-4 bg-white shadow rounded-lg">
            <thead>
                <tr>
                <th className="w-1 font-medium border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    No
                </th>
                <th className="w-52 font-medium border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Nama
                </th>
                <th className="w-52 font-medium border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Mata Kuliah
                </th>
                <th className="w-5 font-medium border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Nilai
                </th>
                <th className="w-5 font-medium border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Indeks Nilai
                </th>
                <th className="w-60 font-medium border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Aksi
                </th>
                </tr>
            </thead>
            <tbody>
                {
                    dataMahasiswa.map((el,index) => {
                        return(
                            <tr className="text-gray-700">
                            <td className="text-center border-b-2 p-4 dark:border-dark-5">
                            {index+1}
                            </td>
                            <td className="text-center border-b-2 p-4 dark:border-dark-5">
                            {el.name}
                            </td>
                            <td className="text-center border-b-2 p-4 dark:border-dark-5">
                            {el.mataKuliah}
                            </td>
                            <td className="text-center border-b-2 p-4 dark:border-dark-5">
                            {el.nilai}
                            </td>
                            <td className="text-center border-b-2 p-4 dark:border-dark-5">
                            {functionIndeksNilai(el.nilai)}
                            </td>
                            <td className="w-44 border-b-2 p-4 dark:border-dark-5">
                                <div className="grid grid-cols-2 gap-1">
                                    <button onClick={handleEdit} value={el.id} className="w-24 py-2 px-4  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">Edit</button>
                                    <button onClick={handleDelete} value={el.id} className="w-24 py-2 px-4  bg-red-600 hover:bg-red-700 focus:ring-red-500 focus:ring-offset-red-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">Delete</button>
                                </div>
                            </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </table> 
        </div>
        </>
    )
}

export default NilaiMahasiswaList;