import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { NilaiMahasiswaContext } from "../Tugas-13/NilaiMahasiswaContext";

const NilaiMahasiswaForm = () => {
    const { inputName, setInputName, inputMataKuliah, setInputMataKuliah, inputNilai, setInputNilai, currentId, functions, setAddMessage } = useContext(NilaiMahasiswaContext);
    const { functionTambah, functionUpdate } = functions;
    
    let history = useHistory()
    const offMessage = () => {
        setTimeout(() => {
            setAddMessage("invisible");
        },3000)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (currentId === null) {
            functionTambah();
        } else {
            functionUpdate();
        }
        setInputName("");
        setInputNilai(0);
        setInputMataKuliah("");
        setAddMessage("visible");
        offMessage();
        history.push("/tugas-15");
    }

    const handleNameChange = (event) => {
        let inputValue = event.target.value;
        setInputName(inputValue);
    }
    const handleNilaiChange = (event) => {
        let inputValue = event.target.value;
        setInputNilai(inputValue);
    }
    const handleMataKuliahChange = (event) => {
        let inputValue = event.target.value;
        setInputMataKuliah(inputValue);
    }
    return (
        <div className="container">
        <form onSubmit={handleSubmit}>
                <label>Nama</label>
                <input className="inputTyp" type="text" value={inputName} onChange={handleNameChange} required/>
                <br/>
                <label>Mata Kuliah</label>
                <input className="inputTyp" type="text"value={inputMataKuliah} onChange={handleMataKuliahChange} required/>
                <br/>
                <label>Nilai</label>
                <input className="inputTyp" type="number" value={inputNilai} onChange={handleNilaiChange} required/>
                <br/>
                <input type="submit"value="Submit" className="submit"/>
            </form>
            </div>
    )
}

export default NilaiMahasiswaForm;