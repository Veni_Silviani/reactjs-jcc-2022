import React, { useState, useEffect} from "react";
import axios from 'axios';
import "./NilaiMahasiswa.css"

const NilaiMahasiswa = () => {
    const [dataMahasiswa, setDataMahasiswa] = useState([]);
    const [inputName, setInputName] = useState("");
    const [inputMataKuliah, setInputMataKuliah] = useState("");
    const [inputNilai, setInputNilai] = useState(0);
    const [currentId, setCurrentId] = useState(null);
    const [fetchStatus, setFetchStatus] = useState(true);
    const handleEdit = (event) => {
        let idMahasiswa = event.target.value;
        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
        .then(res => {
            let data = res.data;
            setInputName(data.name);
            setInputNilai(data.score);
            setInputMataKuliah(data.course);
            setCurrentId(data.id);
        })
    }
    const handleDelete = (event) => {
        let idMahasiswa = event.target.value;
        axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
        .then(() => {
            let newDataMahasiswa = dataMahasiswa.filter((e) => {return e.id !== idMahasiswa});
            setDataMahasiswa(newDataMahasiswa);
            setFetchStatus(true);
        })
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        if (currentId === null) {
            axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name: inputName, course: inputMataKuliah, score: inputNilai})
            .then(res => {
                let data = res.data;
                setDataMahasiswa([...dataMahasiswa, {id: data.id, name: data.name, mataKuliah: data.course, nilai: data.score}]);
            })
        } else {
            axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,{name:inputName,score:inputNilai,course:inputMataKuliah})
            .then(() => {
                let modifiedDataMahasiswa = dataMahasiswa.map(el => {
                    if (el.id === currentId) {
                        el.name = inputName;
                        el.mataKuliah = inputMataKuliah;
                        el.nilai = inputNilai;
                        return el;
                    } else {
                        return el;
                    }
                })
                setDataMahasiswa(modifiedDataMahasiswa);
            })
        }
        setInputName("");
        setInputNilai(0);
        setInputMataKuliah("");
    }
    const handleNameChange = (event) => {
        let inputValue = event.target.value;
        setInputName(inputValue);
    }
    const handleNilaiChange = (event) => {
        let inputValue = event.target.value;
        setInputNilai(inputValue);
    }
    const handleMataKuliahChange = (event) => {
        let inputValue = event.target.value;
        setInputMataKuliah(inputValue);
    }
    useEffect( () => {
        const fetchData = async () => {
          const result = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
          setDataMahasiswa(result.data.map(x=>{ return {id: x.id, name: x.name, mataKuliah: x.course, nilai: x.score} }) )
        }
        fetchData()
        setFetchStatus(false);
      }, [fetchStatus,setFetchStatus])

    const hitungIndeksNilai = (nilai) => {
        if (nilai >= 80) {
            return "A";
        } else if (nilai >= 70 && nilai<80) {
            return "B";
        } else if (nilai >= 60 && nilai<70) {
            return "C";
        } else if (nilai >= 50 && nilai<60) {
            return "D";
        } else {
            return "E";
        }
    }

    return (
        <div className="container">
            <h1 className="heading-tugas12">Daftar Nilai Mahasiswa</h1>
            <table className="e-table">
                <thead className="e-thead">
                    <tr>
                        <td className="e-td">No</td>
                        <td className="e-td">Nama</td>
                        <td className="e-td">Mata Kuliah</td>
                        <td className="e-td">Nilai</td>
                        <td className="e-td">Indeks Nilai</td>
                        <td className="e-td">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataMahasiswa.map((el,index) => {
                            return(
                                <tr>
                                    <td className="e-td">{index+1}</td>
                                    <td className="e-td">{el.name}</td>
                                    <td className="e-td">{el.mataKuliah}</td>
                                    <td className="e-td">{el.nilai}</td>
                                    <td className="e-td">{hitungIndeksNilai(el.nilai)}</td>
                                    <td className="e-td">
                                        <button onClick={handleEdit} value={el.id} className="edit">Edit</button>
                                        <button onClick={handleDelete} value={el.id} className="delete">Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            <h1 className="heading-tugas12">Form Nilai Mahasiswa</h1>
            <form onSubmit={handleSubmit}>
                <label>Nama</label>
                <input className="inputTyp" type="text" value={inputName} onChange={handleNameChange} required/>
                <br/>
                <label>Mata Kuliah</label>
                <input className="inputTyp" type="text"value={inputMataKuliah} onChange={handleMataKuliahChange} required/>
                <br/>
                <label>Nilai</label>
                <input className="inputTyp" type="number" value={inputNilai} onChange={handleNilaiChange} required/>
                <br/>
                <input type="submit"value="Submit" className="submit"/>
            </form>
        </div>    
    )
}

export default NilaiMahasiswa;