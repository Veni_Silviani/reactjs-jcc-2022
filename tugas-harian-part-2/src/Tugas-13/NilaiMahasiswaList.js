import React, { useEffect, useContext} from "react";
import { NilaiMahasiswaContext } from "./NilaiMahasiswaContext";

const NilaiMahasiswaList = () => {
    const { dataMahasiswa, fetchStatus, setFetchStatus, functions } = useContext(NilaiMahasiswaContext)
    const { fetchData, functionDelete, functionEdit, functionIndeksNilai } = functions;
    const handleEdit = (event) => {
        let idMahasiswa = event.target.value;
        functionEdit(idMahasiswa);
    }
    const handleDelete = (event) => {
        let idMahasiswa = event.target.value;
        functionDelete(idMahasiswa);
    }
    useEffect( () => {
        fetchData()
        setFetchStatus(false);
      }, [fetchStatus,setFetchStatus])

    return (
        <div className="container">
            <table className="e-table">
                <thead className="e-thead">
                    <tr>
                        <td className="e-td">No</td>
                        <td className="e-td">Nama</td>
                        <td className="e-td">Mata Kuliah</td>
                        <td className="e-td">Nilai</td>
                        <td className="e-td">Indeks Nilai</td>
                        <td className="e-td">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataMahasiswa.map((el,index) => {
                            return(
                                <tr>
                                    <td className="e-td">{index+1}</td>
                                    <td className="e-td">{el.name}</td>
                                    <td className="e-td">{el.mataKuliah}</td>
                                    <td className="e-td">{el.nilai}</td>
                                    <td className="e-td">{functionIndeksNilai(el.nilai)}</td>
                                    <td className="e-td">
                                        <button onClick={handleEdit} value={el.id} className="edit">Edit</button>
                                        <button onClick={handleDelete} value={el.id} className="delete">Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>    
    )
}

export default NilaiMahasiswaList;