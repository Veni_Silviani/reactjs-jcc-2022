import React from "react";
import { NilaiMahasiswaProvider } from "./NilaiMahasiswaContext";
import NilaiMahasiswaForm from "./NilaiMahasiswaForm";
import NilaiMahasiswaList from "./NilaiMahasiswaList";

const NilaiMahasiswa = () => {
    return (
        <NilaiMahasiswaProvider>
            <h1 className="heading-tugas11">Daftar Nilai Mahasiswa</h1>
            <NilaiMahasiswaList/>
            <h1 className="heading-tugas11">Form Nilai Mahasiswa</h1>
            <NilaiMahasiswaForm/>
        </NilaiMahasiswaProvider>
    )
}

export default NilaiMahasiswa;