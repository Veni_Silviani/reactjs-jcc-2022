import axios from "axios";
import React, { createContext, useState } from "react";

export const NilaiMahasiswaContext = createContext();

export const NilaiMahasiswaProvider = props =>  {
    const [dataMahasiswa, setDataMahasiswa] = useState([]);
    const [inputName, setInputName] = useState("");
    const [inputMataKuliah, setInputMataKuliah] = useState("");
    const [inputNilai, setInputNilai] = useState(0);
    const [currentId, setCurrentId] = useState(null);
    const [fetchStatus, setFetchStatus] = useState(true);
    const [themeSelect, setThemeSelect] = useState("dark");
    const [addMessage, setAddMessage] = useState("invisible");
    
    const fetchData = async () => {
        const result = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
        setDataMahasiswa(result.data.map(x=>{ return {id: x.id, name: x.name, mataKuliah: x.course, nilai: x.score} }) )
     }

     const functionDelete = (idMahasiswa) => {
        axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
        .then(() => {
            let newDataMahasiswa = dataMahasiswa.filter((e) => {return e.id !== idMahasiswa});
            setDataMahasiswa(newDataMahasiswa);
            setFetchStatus(true);
        })
     }

     const functionEdit = (idMahasiswa) => {
        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
        .then(res => {
            let data = res.data;
            setInputName(data.name);
            setInputNilai(data.score);
            setInputMataKuliah(data.course);
            setCurrentId(data.id);
        })
     }

     const functionTambah = () => {
        axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name: inputName, course: inputMataKuliah, score: inputNilai})
        .then(res => {
            let data = res.data;
            setDataMahasiswa([...dataMahasiswa, {id: data.id, name: data.name, mataKuliah: data.course, nilai: data.score}]);
        })
     }

     const functionUpdate = () => {
        axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,{name:inputName,score:inputNilai,course:inputMataKuliah})
            .then(() => {
                let modifiedDataMahasiswa = dataMahasiswa.map(el => {
                    if (el.id === currentId) {
                        el.name = inputName;
                        el.mataKuliah = inputMataKuliah;
                        el.nilai = inputNilai;
                        return el;
                    } else {
                        return el;
                    }
                })
                setDataMahasiswa(modifiedDataMahasiswa);
            })
     }

     const functionIndeksNilai = (nilai) => {
        if (nilai >= 80) {
            return "A";
        } else if (nilai >= 70 && nilai<80) {
            return "B";
        } else if (nilai >= 60 && nilai<70) {
            return "C";
        } else if (nilai >= 50 && nilai<60) {
            return "D";
        } else {
            return "E";
        }
    }

     const functions = {
         fetchData, functionDelete, functionEdit, functionTambah, functionUpdate, functionIndeksNilai
     }

    return (
    <NilaiMahasiswaContext.Provider value={{ 
        dataMahasiswa, setDataMahasiswa,
        inputName, setInputName,
        inputMataKuliah, setInputMataKuliah,
        inputNilai, setInputNilai,
        currentId, setCurrentId,
        fetchStatus, setFetchStatus,
        themeSelect, setThemeSelect,
        addMessage, setAddMessage,
        functions
     }}>
        {props.children}
    </NilaiMahasiswaContext.Provider>
    )
}
