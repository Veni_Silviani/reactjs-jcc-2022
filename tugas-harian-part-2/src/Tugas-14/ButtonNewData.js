import React, { useContext } from "react";
import { useHistory } from "react-router-dom";

const ButtonNewData = () => {
    let history = useHistory();    
    function handleNewDataClick() {
        history.push("/tugas-14/create");
    }
    return (
        <div className="container-tambah-data">
            <button className="tambah-data" type="button" onClick={handleNewDataClick}>Buat Data Nilai Mahaiswa Baru</button>
        </div>        
    )
}

export default ButtonNewData;