import React from "react";
import { Switch, Route, BrowserRouter as Router, Link } from "react-router-dom";
import ToDo from "../Tugas-10/ToDo";
import ManajemenBuah from "../Tugas-11/ManajemenBuah";
import NilaiMahasiswa from "../Tugas-12/NilaiMahasiswa";
import NilaiMahasiswa2  from "../Tugas-13/NilaiMahasiswa";
import { NilaiMahasiswaProvider } from "../Tugas-13/NilaiMahasiswaContext";
import NilaiMahasiswaForm from "../Tugas-13/NilaiMahasiswaForm";
import NilaiMahasiswaList from "../Tugas-13/NilaiMahasiswaList";
import NilaiMahasiswaForm2 from "../Tugas-15/NilaiMahasiswaForm";
import NilaiMahasiswaList2 from "../Tugas-15/NilaiMahasiswaList";
import ButtonChangeMode from "./ButtonChangeMode";
import ButtonNewData from "./ButtonNewData";
import Nav from "./Nav";
import Login from "../auth/login";
import "./tugas14.css";
import { AuthenticationProvider } from "../auth/AuthenticationContext";
import Register from "../auth/register";

const Routes = () => {
    return (
        <Router>
            <NilaiMahasiswaProvider>
            <Nav/>    
            <Switch>
                <Route exact path="/" component={ToDo}/>
                <Route exact path="/tugas-10" component={ToDo}/>
                <Route exact path="/tugas-11" component={ManajemenBuah}/>
                <Route exact path="/tugas-12" component={NilaiMahasiswa}/>
                <Route exact path="/tugas-13" component={NilaiMahasiswa2}/>
                <Route exact path="/tugas-14">
                    <ButtonChangeMode/>
                    <h1 className="heading-tugas11">Daftar Nilai Mahasiswa</h1>
                    <ButtonNewData/>
                    <NilaiMahasiswaList/>
                </Route>
                <Route exact path="/tugas-14/create">
                    <h1 className="heading-tugas11">Form Nilai Mahasiswa</h1>
                    <NilaiMahasiswaForm/>
                </Route>
                <Route exact path="/tugas-15">
                    <NilaiMahasiswaList2/>
                </Route>
                <Route exact path="/tugas-15/create">
                    <NilaiMahasiswaForm2/>
                </Route>
                <AuthenticationProvider>
                <Route exact path="/login">
                    <Login/>
                </Route>
                <Route exact path="/register">
                    <Register/>
                </Route>
                </AuthenticationProvider>
            </Switch>
            </NilaiMahasiswaProvider>
        </Router>
    )
}

export default Routes;
