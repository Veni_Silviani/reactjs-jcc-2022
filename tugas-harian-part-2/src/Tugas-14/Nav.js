import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { NilaiMahasiswaContext } from "../Tugas-13/NilaiMahasiswaContext";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";

const Nav = () => {
    const { themeSelect, setThemeSelect} = useContext(NilaiMahasiswaContext);
    useEffect(() => {
        
    },[])
    return (
        <>
        <ul className={`ul-element ul-${themeSelect}`}>
            <li className={`li-element hover-${themeSelect}`}>
                <Link className={`a-element a-${themeSelect}`} to="/tugas-10">Tugas 10</Link>
            </li>
            <li className={`li-element hover-${themeSelect}`}>
                <Link className={`a-element a-${themeSelect}`} to="/tugas-11">Tugas 11</Link>
            </li>
            <li className={`li-element hover-${themeSelect}`}>
                <Link className={`a-element a-${themeSelect}`} to="/tugas-12">Tugas 12</Link>
            </li>
            <li className={`li-element hover-${themeSelect}`}>
                <Link className={`a-element a-${themeSelect}`} to="/tugas-13">Tugas 13</Link>
            </li>
            <li className={`li-element hover-${themeSelect}`}>
                <Link className={`a-element a-${themeSelect}`} to="/tugas-14">Tugas 14</Link>
            </li>
            <li className={`li-element hover-${themeSelect}`}>
                <Link className={`a-element a-${themeSelect}`} to="/tugas-15">Tugas 15</Link>
            </li>
                {!Cookies.get('token') && <>
                    <li className={`li-element hover-${themeSelect}`}>
                        <Link className={`a-element a-${themeSelect}`} to="/login">Login</Link>
                    </li>
                    <li className={`li-element hover-${themeSelect}`}>
                        <Link className={`a-element a-${themeSelect}`} to="/register">Register</Link>
                    </li>
                </>}
                {Cookies.get('token') && <>
                    <li className={`li-element hover-${themeSelect}`}>
                        <span onClick={() => {
                            Cookies.remove('token');
                            window.location = "/login";
                        }}>Logout</span>
                    </li>
                </>}
        </ul>
        </>
    )
}

export default Nav;