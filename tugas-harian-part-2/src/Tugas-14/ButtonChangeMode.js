import React, { useContext, useState } from "react";
import { NilaiMahasiswaContext } from "../Tugas-13/NilaiMahasiswaContext";
const ButtonChangeMode = () => {
    const [theme,setTheme] = useState("Dark Theme");
    const { themeSelect, setThemeSelect} = useContext(NilaiMahasiswaContext);
    function handleClick() {
        if (theme === "Dark Theme") {
            setTheme("Light Theme");
            setThemeSelect("light");
        } else {
            setTheme("Dark Theme");
            setThemeSelect("dark");
        }
    }
    return (
        <div>
            <button className="change-mode" type="button" onClick={handleClick}>Change Navbar to {theme}</button>
        </div>
    )
}

export default ButtonChangeMode;