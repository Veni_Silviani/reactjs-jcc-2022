import axios from "axios";
import React, { createContext, useState } from "react";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";

export const AuthenticationContext = createContext();

export const AuthenticationProvider = props => {
    const history = useHistory();
    const [input, setInput] = useState({
        email : "",
        password : "",
        name : "",
    });
    const handleChange = (event) => {
        
        let value = event.target.value
        let name = event.target.name
        setInput({...input, [name] : value})
    }
    const handleLogin = (event) => {
        event.preventDefault()
        let {email, password} = input
        axios.post(`https://backendexample.sanbersy.com/api/user-login`, {email, password})
        .then((res) => {
            let {token} = res.data
            Cookies.set('token', token)
            setInput({...input, email : "", password : ""})
            window.location = "/";
        })
    }
    const handleRegister = (event) => {
        event.preventDefault()
        let {name, email, password} = input
        axios.post(`https://backendexample.sanbersy.com/api/register`, {name, email, password})
        .then(() => {
            setInput({...input, email : "", password : "", name : ""})
            history.push('/login')
        })

    }
    const functions = {
        handleChange, handleLogin, handleRegister
    }
    return (
        <AuthenticationContext.Provider value={{ 
            input, setInput,
            functions,
         }}>
             {props.children}
        </AuthenticationContext.Provider>
    )
}