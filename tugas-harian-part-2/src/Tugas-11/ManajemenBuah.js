import React,{ useState } from "react";
import './ManajemenBuah.css';

const ManajemenBuah = () => {
    let daftarBuah =  [
        {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
        {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
        {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
        {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
    ];
    const [dataBuah,setDataBuah] = useState(daftarBuah);
    const [inputNama, setInputNama] = useState("");
    const [inputHarga, setInputHarga] = useState(0);
    const [inputBerat, setInputBerat] = useState(0);
    const [message,setMessage] = useState("");
    const [currentIndex, setCurrentIndex] = useState(-1);
    const handleNamaChange = (el) => {
        setInputNama(el.target.value);
    }
    const handleHargaChange = (el) => {
        setInputHarga(el.target.value);
    }
    const handleBeratChange = (el) => {
        setInputBerat(el.target.value);
    }
    const handleDelete = (el) => {
        let index = el.target.value;
        let deletedData = dataBuah[index].nama;
        let newData = dataBuah.filter((e) => {
            return e.nama !== deletedData; 
        });
        setDataBuah(newData);
    }
    const handleEdit = (el) => {
        let index = el.target.value;
        let editValue = dataBuah[index];
        setInputNama(editValue.nama);
        setInputHarga(editValue.hargaTotal);
        setInputBerat(editValue.beratTotal);
        setCurrentIndex(index);
    }
    const handleSubmit = (el) => {
        el.preventDefault();
        let modifiedData = dataBuah;
        if (inputNama === "") {
            setMessage("*Masukan nama buah*");
        } else if(inputHarga === 0) {
            setMessage("*Masukan harga total buah*");
        } else if(inputBerat < 2000) {
            setMessage("*Masukan berat minimal 2000 g (2 Kg)*")
        } else if (currentIndex === -1) {
            let newData = {
                nama: inputNama,
                hargaTotal: inputHarga,
                beratTotal: inputBerat
            }
            modifiedData = [...dataBuah,newData];
            setInputNama("");
            setInputHarga(0);
            setInputBerat(0);
            setMessage("");
        } else {
            modifiedData[currentIndex].nama = inputNama;
            modifiedData[currentIndex].hargaTotal = inputHarga;
            modifiedData[currentIndex].beratTotal = inputBerat;
            setCurrentIndex(-1);
            setInputNama("");
            setInputHarga(0);
            setInputBerat(0);
            setMessage("");
        }
        setDataBuah(modifiedData);
    }
    return (
        <div className="container">
            <h1 className="heading-tugas11">Daftar Harga Buah</h1>
            <table className="e-table">
                <thead className="e-thead">
                    <tr>
                        <td className="e-td">No</td>
                        <td className="e-td">Nama</td>
                        <td className="e-td">Harga Total</td>
                        <td className="e-td">Berat Total</td>
                        <td className="e-td">Harga per Kg</td>
                        <td className="e-td">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataBuah.map((el,index) => {
                            return(
                                <tr>
                                    <td className="e-td">{index+1}</td>
                                    <td className="e-td">{el.nama}</td>
                                    <td className="e-td">{el.hargaTotal}</td>
                                    <td className="e-td">{el.beratTotal}</td>
                                    <td className="e-td">{el.hargaTotal/(el.beratTotal/1000)}</td>
                                    <td className="e-td">
                                        <button onClick={handleEdit} value={index} className="edit">Edit</button>
                                        <button onClick={handleDelete} value={index} className="delete">Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            <h1 className="heading-tugas11">Form Daftar Harga Buah</h1>
            <form onSubmit={handleSubmit}>
                <label>Nama</label>
                <input className="inputTyp" type="text" value={inputNama} onChange={handleNamaChange}/>
                <br/>
                <label>Harga Total</label>
                <input className="inputTyp" type="number"value={inputHarga} onChange={handleHargaChange}/>
                <br/>
                <label>Berat Total (dalam gram)</label>
                <input className="inputTyp" type="number" value={inputBerat} onChange={handleBeratChange}/>
                <br/>
                <label className="message">{message}</label>
                <input type="submit"value="Submit" className="submit"/>
            </form>
        </div>
    )
}

export default ManajemenBuah;