import React from "react";
import LogoJCC from "../assest/img/logo.png";
import "./ToDo.css";
const ListCheckbox = (props) => {
    return (
        <div className="List">
        <input type={'checkbox'} /> {props.text}
        </div>
    )
}

const ToDo = () => {
    return (
    <>
    <div className="card-tugas10">
      <img src={LogoJCC} />
      <h1>THINGS TO DO</h1>
      <p>During bootcamp in jabarcodingcamp</p>
      <hr/>

      <div className="container-list">
        <ListCheckbox text={"Belajar GIT & CLI"}/>
        <ListCheckbox text={"Belajar HTML & CSS"}/>
        <ListCheckbox text={"Belajar Javascript"}/>
        <ListCheckbox text={"Belajar ReactJS Dasar"}/>
        <ListCheckbox text={"Belajar ReactJD Advance"}/>
        <button class="submit">Send</button>
      </div>
    </div>
    </>
    )
}

export default ToDo;