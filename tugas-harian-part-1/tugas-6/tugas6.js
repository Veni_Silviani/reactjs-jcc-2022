/*soal 1 ( Membuat 2 Conditional ) */
var nilaiJohn = 80;
var nilaiDoe = 50;

if (nilaiJohn >= 80) {
    console.log("index nilai jhon adalah A");
} else if (nilaiJhon>= 70 && nilaiJohn < 80) {
    console.log("index nilai jhon adalah B");
} else if (nilaiJhon >= 60 && nilaiJohn < 70) {
    console.log("index nilai jhon adalah C");
} else if (nilaiJhon >= 50 && nilaiJohn < 60) {
    console.log("index nilai jhon adalah D");
} else {
    console.log("index nilai jhon adalah E");
}

if (nilaiDoe >= 80) {
    console.log("index nilai Doe adalah A");
} else if (nilaiDoe >= 70 && nilaiDoe < 80) {
    console.log("index nilai Doe adalah B");
} else if (nilaiDoe >= 60 && nilaiDoe < 70) {
    console.log("index nilai Doe adalah C");
} else if (nilaiDoe >= 50 && nilaiDoe < 60) {
    console.log("index nilai Doe adalah D");
} else {
    console.log("index nilai Doe adalah E");
}

// soal 2 ( membuat conditional menggunakan switch case )
var tanggal = 06;
var bulan = 12;
var tahun = 1998;

var namaBulan = "" ;
switch(bulan){
    case 1: { 
        namaBulan = 'januari' ;
        break;
    }
    case 2: {
        namaBulan = 'februari' ;
        break;
    }
    case 3: {
        namaBulan = 'maret' ;
        break ;
    }
    case 4: {
        namaBulan = 'april' ;
        break ;
    }
    case 5: {
        namaBulan = 'mei'
        break;
    }
    case 6: {
        namaBulan = 'juni'
        break;
    }
    case 7: {
        namaBulan = 'juli'
        break;
    }
    case 8: {
        namaBulan = 'agustus'
        break;
    }
    case 9: {
        namaBulan = 'september'
        break;
    }
    case 10: {
        namaBulan = 'oktober'
        break;
    }
    case 11: {
        namaBulan = 'november'
        break;
    }
    default: {
        namaBulan = 'desember'
        break;
    }
}
console.log(`${tanggal} ${namaBulan} ${tahun}`);

// soal 3 ( 2 Looping  Sederhana )
console.log("LOOPING PERTAMA");
for (let i=2;i<=20;i+=2) {
    console.log(`${i} - I love coding`);
}
console.log("LOOPING KEDUA");
for (let i=20;i>=2;i-=2) {
    console.log(`${i} - I will become a frontend developer`);
}

// soal 4 ( Looping dengan conditional )
console.log('');
for (let i=1;i<=20;i++) {
    if (i % 3 == 0 && i % 2 == 1){
        console.log(`${i} - I Love Coding`);
    } else if (i % 2 ==1){
        console.log(`${i} - Santai`);
    } else {
        console.log(`${i} - Berkualitas`);
    }
}

// soal 5 ( Segitita Looping )
var pagar = '';
for (let i=1;i<=7;i++) {
    pagar+='#';
    console.log (pagar);

    
}

