// soal 1 ( Membuat Function dengan rumus )
let luasPersegiPanjang = (p, l) =>{
    return p*l ;
}
let kelilingPersegiPanjang = (p, l) => {
    return 2*(p+l) ;
}
let volumeBalok = (p, l, t) => {
    return p*l*t ;
}
let panjang= 12 ;
let lebar= 4;
let tinggi = 8 ;
 
let HasilluasPersegiPanjang = luasPersegiPanjang(panjang, lebar);
let HasilkelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar);
let HasilvolumeBalok = volumeBalok(panjang, lebar, tinggi);

console.log(HasilluasPersegiPanjang ) ;
console.log(HasilkelilingPersegiPanjang ) ;
console.log(HasilvolumeBalok ) ;

console.log();

// Soal 2 ( Membuat Function return String + rest parameter )
const introduce = (...rest) => {
    let [name, age, gender, hobby] = rest
    let i = "undefined" 
    if (gender === "Laki-Laki") {
        i = "Pak" ;
    } else {
        i = "Buk" ;
    }
    return `${i} ${name} adalah penulis yang berusia 30 tahun`;
}

//kode di bawah ini jangan dirubah atau dihapus

const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
console.log()

// soal 3 ( mengubah array menjadi object )
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
let objDaftarPeserta = {
    nama: arrayDaftarPeserta[0],
    gender: arrayDaftarPeserta[1],
    hobby: arrayDaftarPeserta[2],
    tahun: arrayDaftarPeserta[3]
}
console.log(objDaftarPeserta)
console.log()

// soal 4 ( Membuat sebuah array of object dan melakukan filter )
var buah = [
{ nama: 'Nanas',
  warna: 'Kuning',
  adaBijinya: false,
  harga: 9000 },
{ nama: 'Jeruk',
  warna: 'Oranye',
  adaBijinya: true,
  harga: 8000 },
{ nama: 'Semangka',
  warna: 'Hijau & Merah',
  adaBijinya: true,
  harga: 10000 },
{ nama: 'Pisang',
  warna: 'Kuning',
  adaBijinya: false,
  harga: 5000 }
]
let output = buah.filter((respon) => {
    return respon.adaBijinya === false;
} )
console.log(output)
console.log() 

// Soal 5 ( Destructuring pada Object )
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
 /* Tulis kode jawabannya di sini */
 let {name:phoneName, brand:phoneBrand, year:year, colors:colors} = phone;
 let [colorBronze, colorWhite, colorBlack] = colors;
 // kode di bawah ini jangan dirubah atau dihapus
 console.log(phoneBrand, phoneName, year, colorBlack, colorBronze)
 console.log() 

//  soal 6 ( Spred Operator pada Object )
 let warna = ["biru", "merah", "kuning" , "hijau"]

 let dataBukuTambahan= {
   penulis: "john doe",
   tahunTerbit: 2020 
 }
 
 let buku = {
   nama: "pemograman dasar",
   jumlahHalaman: 172,
   warnaSampul:["hitam"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
 /* Tulis kode jawabannya di sini */ 
 let book = {...buku, ...dataBukuTambahan};
 book.warnaSampul = ["hitam", ...warna];
 console.log(book);

 console.log()


//  soal 7 (  membuat function yang mengisi data kedalam array kosong )
 /* 
    Tulis kode function di sini 
*/
function tambahDataFilm(nama,durasi,genre,tahun) {
    let obj = {
        "nama": nama,
        "durasi":durasi,
        "genre":genre,
        "tahun":tahun
    }
    dataFilm.push(obj);
}

let dataFilm = []

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")
console.log(dataFilm)