// Soal 1 ( Destructuring Array )
let dataPeserta = ["john", "laki-laki", "programmer", "30"]
const [nama, gender, pekerjaan, umur] = dataPeserta;
console.log(`hallo, nama saya ${nama}. saya ${gender} berumur ${umur} bekerja sebagai seorang ${pekerjaan}`);
console.log();

// Soal 2 ( Mengeluarkan element array )
let array1 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]
for (var i = 0; i < array1.length;i++){
    console.log(array1[i]);
}
console.log()

// Soal 3 ( Mengeluarkan element array dan dengan kondisi )
let array2 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]
for (let i=0;i<=9;i++){
    if (array2[i] % 2 == 0){
        console.log(array2[i]);  
    }
    
}
console.log()

// Soal 4 ( Menghilangkan element dan menggabungkan element menjadi string )
let kalimat= ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.splice(0,3, 'saya')
console.log(kalimat.join(' '));

console.log()

// Soal 5 ( Menambahkan, Mengurutkan dan mengeluarkan element array )
var sayuran=["kangkung", "bayam", "buncis", "kubis", "seledri", "tauge", "timun"]
sayuran.sort()
for(i = 0;i < sayuran.length; i++){
    console.log(i+1 +'. '+ sayuran[i]);
}

