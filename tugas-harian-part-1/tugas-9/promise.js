// Soal 2    ( menjalankan promise menggunakan then&catch)

function readBooksPromise (time, book) {
    console.log("saya mulai membaca " + book.name )
    return new Promise( function (resolve, reject){
      setTimeout(function(){
        let sisaWaktu = time - book.timeSpent
        if(sisaWaktu >= 0 ){
            console.log("saya sudah selesai membaca " + book.name + ", sisa waktu saya " + sisaWaktu)
            resolve(sisaWaktu)
        } else {
            console.log("saya sudah tidak punya waktu untuk baca "+ book.name)
            reject(sisaWaktu)
        }
      }, book.timeSpent)
    })
  }

  var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise ( menggunakan promise ) 
let i = 0;
function bacaBuku(waktu) {
    readBooksPromise(waktu,books[i++])
    .then(function(sisaWaktu) {
        bacaBuku(sisaWaktu);
    })
    .catch(function(sisaWaktu) {
        
    })    
}
bacaBuku(10000);