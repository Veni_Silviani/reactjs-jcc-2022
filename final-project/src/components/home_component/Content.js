import React from "react";
import Pagination from "./Pagination";

export default function Content() {
    return(
        <div className="my-5 p-0 w-full grid md:grid-cols-4 sm:grid-cols-2 gap-5">
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overflow-hidden drop-shadow-md rounded-2xl h-90 w-60 md:w-full cursor-pointer m-auto">
                <a href="#" className="w-full block h-full">
                    <img alt="blog photo" src="https://www.nttdata.com/id/en/-/media/nttdataapac/ndid/career/careers-header-2732x1536-2.jpg" className="max-h-40 w-full object-cover" />
                </a>
                <div className="bg-white dark:bg-gray-800 w-full p-4">
                    <a href="#" className="w-full block h-full">
                        <p className="text-purple-500 text-md font-medium">
                            Dicari
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                            ReactJS Programmer
                        </p>
                        <p className="text-gray-400 dark:text-gray-300 font-light text-md">
                            The new supercar is here, 543 cv and 140 000$. This is best racing GT about 7 years on...
                        </p>
                    </a>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img alt="profil" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzHQv_th9wq3ivQ1CVk7UZRxhbPq64oQrg5Q&usqp=CAU" className="mx-auto object-cover rounded-full h-10 w-10 " />
                        </a>
                        <div className="flex flex-col justify-between ml-4 text-sm">
                            <p className="text-gray-800 dark:text-white">
                                Jean Jacques
                            </p>
                            <p className="text-gray-400 dark:text-gray-300">
                                20 mars 2029 - 6 min read
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}