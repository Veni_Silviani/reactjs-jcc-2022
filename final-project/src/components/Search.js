import React from "react";
import Content from "./home_component/Content";
import Footer from "./home_component/Footer";
import Nav from "./home_component/Nav";
import Pagination from "./home_component/Pagination";
import Filter from "./search_component/Filter";

export default function Search() {
    return (
        <div className="m-5">
            <Nav/>
            <Filter/>
            <Content/>
            <Pagination/>
            <Footer/>
        </div>
    )
}