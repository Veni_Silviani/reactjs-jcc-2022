import React from "react";
import { AdjustmentsIcon, SwitchVerticalIcon } from '@heroicons/react/outline';
import { Menu } from '@headlessui/react'

export default function Filter() {
    return (
        <>
            <Menu>
                <div className="w-28">
                    <Menu.Button className="py-2 flex justify-center items-center  bg-purple-600 hover:bg-purple-700 focus:ring-purple-500 focus:ring-offset-purple-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
                        <AdjustmentsIcon className="h-5 w-auto"/>
                        Filter
                    </Menu.Button>
                </div>
                <Menu.Items>
                    <div className="my-2 w-full flex items-center">
                    <Menu.Item>
                        {({ active }) => (
                            <div className="w-28 md:w-1/6 mr-2">
                            <button className="py-2 flex justify-center items-center  bg-gray-50 hover:bg-gray-200 focus:ring-gray-50 focus:ring-offset-gray-50 text-gray-500 w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
                                <SwitchVerticalIcon className="h-5 w-auto mr-1 drop-shadow-md"/>
                                Nama
                            </button>
                            </div>
                        )}
                    </Menu.Item>
                    <Menu.Item>
                        {({ active }) => (
                            <div className="w-28 md:w-1/6 mr-2">
                            <button className="py-2 flex justify-center items-center  bg-gray-50 hover:bg-gray-200 focus:ring-gray-50 focus:ring-offset-gray-50 text-gray-500 w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
                                <SwitchVerticalIcon className="h-5 w-auto mr-1 drop-shadow-md"/>
                                Terbaru
                            </button>
                            </div>
                        )}
                    </Menu.Item>
                    </div>
                </Menu.Items>
            </Menu>
        </>
    )
}