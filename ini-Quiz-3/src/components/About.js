import React from "react";
import logo from '../assets/img/logoJCC.png';

export default function About() {
    return (
        <div>
            <img src={logo} alt="logo" width="200px" />
            <h1>Jabarcodingcamp 2022 | Frontend Web Development</h1>
            <p>Hallo! Saya <b>Veni Silviani</b>, dari Jabarcodingcamp batch ke-2</p>
            <hr />
            <h3>Data Peserta Jabarcodingcamp Reactjs</h3>
            <ul>
            <li>Nama: Veni Silviani</li>
            <li>Email: venisilviani123@gmail.com</li>
            <li>Sistem Operasi : windows</li>
            <li>Akun Gitlab: Veni_Silviani</li>
            <li>Akun Telegram: Veni Silviani</li>
            </ul>
        </div>
    )
}