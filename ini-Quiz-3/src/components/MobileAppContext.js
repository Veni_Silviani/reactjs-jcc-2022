import React, { createContext, useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";

export const MobileAppContext = createContext();

export  function MobileAppProvider(props) {
    const [input, setInput] = useState(
        {
            name: "",
            description: "",
            category: "",
            release_year: 2007,
            size: 0,
            price: 0,
            rating: 0,
            image_url: "",
            is_android_app: 1,
            is_ios_app: 1
        }
    );

    const [mobileApps, setMobileApps] = useState([]);
    const [currentId, setCurrentId] = useState(null);
    function handleSubmit(event) {
        event.preventDefault();
        if (currentId === null) {
            axios.post("https://backendexample.sanbercloud.com/api/mobile-apps", {
                name: input.name,
                description: input.description,
                category: input.category,
                size:input.size,
                price:input.price,
                rating:input.rating,
                image_url:input.image_url,
                release_year:input.release_year,
                is_android_app: input.is_android_app,
                is_ios_app: input.is_ios_app
            })
            .then(res => {
                let x = res.data;
                setMobileApps([...mobileApps, {
                    id: x.id,
                    name: x.name,
                    description: x.description,
                    category: x.category,
                    size:x.size,
                    price:x.price,
                    rating:x.rating,
                    image_url:x.image_url,
                    release_year:x.release_year,
                    is_android_app: x.is_android_app,
                    is_ios_app: x.is_ios_app
                }]);
            })
        } else {
            axios.put(`https://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`,{
                name: input.name,
                description: input.description,
                category: input.category,
                size:input.size,
                price:input.price,
                rating:input.rating,
                image_url:input.image_url,
                release_year:input.release_year,
                is_android_app: input.is_android_app,
                is_ios_app: input.is_ios_app
            })
            .then(() => {
                let NewData = mobileApps;
                NewData.map((arr) => {
                    if (arr.id === currentId) {
                        arr.name = input.name;
                        arr.description = input.description;
                        arr.category = input.category;
                        arr.size = input.size;
                        arr.price = input.price;
                        arr.rating = input.rating;
                        arr.image_url =input.image_url;
                        arr.release_year = input.release_year;
                        arr.is_android_app = input.is_android_app;
                        arr.is_ios_app = input.is_ios_app;
                    } else {
                        return arr;
                    }
                });
            })
        }
        setInput({
            name: "",
            description: "",
            category: "",
            release_year: 2007,
            size: 0,
            price: 0,
            rating: 0,
            image_url: "",
            is_android_app: 1,
            is_ios_app: 1
        });
        setCurrentId(null);
    }
    function handleDelete(e) {
        let idApp = parseInt(e.target.value);
        axios.delete(`https://backendexample.sanbercloud.com/api/mobile-apps/${idApp}`)
        .then(() => {
            let newData = mobileApps.filter((el) => {
                return el.id !== idApp;
            })
            setMobileApps(newData);
        });
    }
    function handleEdit(idApp) {
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idApp}`)
        .then(res => {
            let data = res.data;
            setInput({
                name: data.name,
                description: data.description,
                category: data.category,
                release_year: data.release_year,
                size: data.size,
                price: data.price,
                rating: data.rating,
                image_url: data.image_url,
                is_android_app: data.is_android_app,
                is_ios_app: data.is_ios_app
            });
            setCurrentId(idApp);
        });
    }
    function handleChange(event) {
        let {name, value} = event.target;
        setInput({...input, [name]:value});
    }
    async function fetchData() {
        const result = await axios.get("http://backendexample.sanbercloud.com/api/mobile-apps");
        setMobileApps(result.data.map(x => {
            return {
                id: x.id,
                name: x.name,
                description: x.description,
                category: x.category,
                size:x.size,
                price:x.price,
                rating:x.rating,
                image_url:x.image_url,
                release_year:x.release_year,
                is_android_app: x.is_android_app,
                is_ios_app: x.is_ios_app
            }
        }))
    }

    const functions = {
        handleSubmit, handleChange, fetchData, handleEdit, handleDelete
    }

    return (
        <MobileAppContext.Provider value={
            {
                input, setInput,
                mobileApps, setMobileApps,
                currentId,setCurrentId,
                functions
            }
        }>
            {props.children}
        </MobileAppContext.Provider>
    )
}