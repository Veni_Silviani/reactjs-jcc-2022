import React from "react";
import { Link } from "react-router-dom";
import logo from '../assets/img/logo.png';
export default function Nav() {
    return (
        <>
        <div className="flex bg-sky-500 p-2 text-slate-50">
            <Link to="/" className="mx-3">
                <img className="inline" src={logo} width={70} />
            </Link >
            <Link to="/" className="mx-3">Home</Link>
            <Link to="/mobile-list" className="mx-3">Mobile List</Link>
            <Link to="/about" className="mx-3">About</Link>
            <form className="mx-3">
                <input type="text" className="mx-3 rounded-lg"/>
                <input type="submit" defaultValue="Cari" />
            </form>
        </div>
        </>
    )
}