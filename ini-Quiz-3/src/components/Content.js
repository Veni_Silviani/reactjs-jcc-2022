import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import About from "./About";
import DataList from "./DataList";
import Edit from "./Edit";
import Footer from "./Footer";
import Form from "./Form";
import Home from "./Home";
import { MobileAppProvider } from "./MobileAppContext";
import Nav from "./Nav";
import Search from "./Search";

export default function Content() {
    return (
        <>
        <Router>
            <MobileAppProvider>
            <Nav/>
            <div className="mx-auto w-[50rem]">
            <Switch>
                <Route exact path="/mobile-list" component={DataList}/>
                <Route exact path="/mobile-form" component={Form}/>
                <Route exact path="/mobile-form/edit/:id" component={Edit}/>
                <Route exact path="search/:valueOfSearch" component={Search}/>
                <Route exact path="/about" component={About}/>
                <Route path="/" component={Home}/>
            </Switch>
            </div>
            <Footer/>
            </MobileAppProvider>
        </Router>
        </>
    )
}