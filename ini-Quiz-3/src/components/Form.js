import React, { useContext } from "react";
import {MobileAppContext} from "./MobileAppContext";

export default function Form() {
    const {input, setInput,
        functions} = useContext(MobileAppContext);
    
    const {handleSubmit, handleChange} = functions;
    
    return (
        <>
        <h1>Mobile Apps Form</h1>
        <form onSubmit={handleSubmit}>
            <div>
                <label>Name</label>
                <input type="text" name="name" className="" value={input.name} onChange={handleChange} required></input>
            </div>
            <div>
                <label>Category</label>
                <input type="text" name="category" className="" value={input.category} onChange={handleChange} required></input>
            </div>
            <div>
                <label>Description</label>
                <textarea name="description" className="" value={input.description} onChange={handleChange} required></textarea>
            </div>
            <div>
                <label>Release Year</label>
                <input type="number" name="release_year" className="" min="2007" max="2021" value={input.release_year} onChange={handleChange} required></input>
            </div>
            <div>
                <label>Size (MB)</label>
                <input type="number" name="size" className="" value={input.size} onChange={handleChange} required/>
            </div>
            <div>
                <label>Price</label>
                <input type="number" name="price" className="" value={input.price} onChange={handleChange} required/>
            </div>
            <div>
                <label>Rating</label>
                <input type="number" name="rating" min="0" max="5" className="" value={input.rating} onChange={handleChange} required/>
            </div>
            <div>
                <label>Image URL</label>
                <input type="text" name="image_url" className="" value={input.image_url} onChange={handleChange} required/>
            </div>
            <div>
                <label>Compability</label>
                <div>
                    <label>Android</label>
                    <input type="checkbox" name="is_android_app" value={input.is_android_app}/>
                </div>
                <div>
                    <label>IOS</label>
                    <input type="checkbox" name="is_ios_app" value={input.is_ios_app}/>
                </div>
            </div>
            <input type="submit" value="Submit"/>
        </form>
        </>
    )
}