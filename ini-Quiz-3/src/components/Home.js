import React, { useContext, useEffect } from "react";
import { MobileAppContext } from "./MobileAppContext";

export default function Home() {
    const {mobileApps, setMobileApps, functions} = useContext(MobileAppContext);
    const {fetchData} = functions;
    useEffect((fetchData),[]);
    return (
        <>
        {
            mobileApps.map((item) => {
                let size = "";
                if (item.size >= 1000) {
                    size = `${item.size/1000} GB`;
                } else {
                    size = `${item.size} MB`
                }
                return(
                    <div className="row">
                        <div className="section">
                            <div className="card">
                            <div>
                                <h2 className="font-bold from-neutral-800 text-4xl">{item.name}</h2>
                                <h5>Release Year : {item.release_year}</h5>
                                <div className="rounded-lg">
                                    <img classname="w-fit" style={{width: '50%', height: '300px', objectFit: 'cover'}} src={item.image_url} />
                                </div>
                                <br />
                                <br />
                                <div>
                                <strong>Price: {(item.price == 0 ? "Free" : item.price)}</strong><br />
                                <strong>Rating: {item.rating}</strong><br />
                                <strong>Size: {size}</strong><br />
                                <strong style={{marginRight: '10px'}}>Platform : Android &amp; IOS
                                </strong>
                                <br />
                                </div>
                                <p>
                                <strong style={{marginRight: '10px'}}>Description :</strong>
                                {item.description}
                                </p>
                                <hr />
                            </div>
                            </div>
                        </div>
                    </div>
                )
            })
        }
        </>
    )
}
