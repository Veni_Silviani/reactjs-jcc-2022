import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { MobileAppContext } from "./MobileAppContext";

export default function DataList() {
    const {mobileApps, setMobileApps, functions} = useContext(MobileAppContext);
    const {fetchData, handleDelete} = functions;
    const history = useHistory()
    function editData(e) {
        history.push(`/mobile-form/edit/${e.target.value}`)
    }
    useEffect((fetchData),[]);
    return (
        <>
        <table>
            <thead className="bg-slate-400 text-slate-200">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Release Year</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Rating</th>
                    <th>Platform</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                mobileApps.map((item, index) => {
                    return(
                        <tr>
                            <td>{index+1}</td>
                            <td>{item.name}</td>
                            <td>{item.category}</td>
                            <td>{item.description}</td>
                            <td>{item.release_year}</td>
                            <td>{item.size}</td>
                            <td>{item.price}</td>
                            <td>{item.rating}</td>
                            <td>{item.is_android_app}
                            {item.is_ios_app}
                            </td>
                            <td>
                                <button onClick={editData} value={item.id}>Edit</button>
                                <button onClick={handleDelete} value={item.id}>Delete</button>
                            </td>
                        </tr>
                    )
                })
                }
            </tbody>
            </table>
        </>
    )
}